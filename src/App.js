import axios from 'axios';
import { useEffect ,useState} from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
// import { Button, Table } from 'react-bootstrap';
import Table from 'react-bootstrap/Table'
import './App.css';

function App() {
  const [api_data,setApi_data]=useState([])
  const [page,setPage]= useState (1)
  const [hasMoreData , setHasMoreData] = useState(true)
  // dateDMY: Moment("1994-07-01").format('DD-MM-YYYY'),
  useEffect(()=>{
    if(hasMoreData) {
      axios.get(`https://api.stackexchange.com/2.2/search/advanced?page=${page}&pagesize=20&o%20rder=desc&sort=activity&site=stackoverflow`).then(res=>{
        if(res.data.items.length == 0) {
          setHasMoreData(false);
        }
        setApi_data([...api_data,...res.data.items])  
      });
    }
  },[page])
 const dformat = (d) => {
    var date = new Date(d);
    var newD = date.getDate()+
    "/"+(date.getMonth()+1)+
    "/"+date.getFullYear()+
    " "+date.getHours()+
    ":"+date.getMinutes()+
    ":"+date.getSeconds()
    return newD
  }
 
  return (
    <div className="App">
      <h1>Question List</h1>
      <InfiniteScroll dataLength={api_data.length}
      next={()=>setPage(page+1)}
      hasMore={hasMoreData}
      loader={<h4>Fetching Data...</h4>}>
      
      <Table striped bordered hover size="sm">
      <thead>
              <tr>
                <th>Author</th>
                <th>Title</th>
                <th>Creation Date</th>
                </tr>
                </thead>
                <tbody>
      {api_data.map((m) => {
        return(
          <tr><td><h4>{m.owner.display_name}</h4></td>
          <td><h4>{m.title}</h4></td>
          <td><h4>
            {dformat(m.creation_date)}</h4></td>
          </tr>
          
)

      })}</tbody></Table>
      </InfiniteScroll>
      {/* {console.log(api_data)} */}
    </div>
  );
}

export default App;
